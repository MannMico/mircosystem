// const path = require('path')
// const { name } = require('../package.json')
// module.exports = {
//     devServer:{
//         port:'3001',
//         headers:{
//             'Access-Control-Allow-Origin':'*',
//             'Access-Control-Allow-Headers':'Content-Type'
//         }
//     },
//     publicPath:'/sub-vue',
//     configuerWebpack:{
//         output:{
//             library: `sub-vue`,
//             libraryTarget: 'umd',
//             jsonpFunction:`webpackJsonp_${name}`,
//             globalObject:'this'
//         }
//     }
// }

const path = require('path');
const packageName = require('./package').name;

function resolve(dir) {
  return path.join(__dirname, dir);
}

const port = 7001; // dev port
module.exports = {

  publicPath:`//localhost:${port}`,
  outputDir: 'dist',
  assetsDir: 'static',
  filenameHashing: true,
  lintOnSave:false,
  devServer: {
    // host: '0.0.0.0',
    hot: true,
    historyApiFallback: true,//添加 重点
    port,
    overlay: {
      warnings: false,
      errors: true,
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },

  configureWebpack: {
    resolve: {
      alias: {
        '@': resolve('src'),
      },
    },
    output: {
      library: `${packageName}-[name]`,
      libraryTarget: 'umd',
      jsonpFunction: `webpackJsonp_${packageName}`,
    },
  },
};
