const routes = [
    {
        path: '/404',
        component: () => import(/* webpackChunkName: "fail" */ '@/views/404')
    },
]
export default routes